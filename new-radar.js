var cfg = {
	width: 1000,
	height: 1000,

	YOffset: 40,

	labelOffset: 10,
	labelSize: "30px",
	labelFamily: "sans-serif",

	blipRadius: 25,

	arcSpan: 75,

	fadeOpacity: 0.3,
	fullOpacity: 1,
}

var arcs;

function getArc(name)
{
	for ( i in arcs )
	{
		if ( arcs[i].name == name ) return arcs[i];
	}

	return null;
}

var svg, arcPathFn;

function setup() {
	svg = d3.select("svg.svg-container")
				.attr("viewBox", "0 0 " + cfg.width + " " + cfg.height)
				.attr("width", cfg.width)
				.attr("height", cfg.height);

	svg.append("svg:g")
		.attr("class", "arcs");

	arcPathFn = d3.svg.arc()
				.innerRadius(function(d, i) { return d.inner })
				.outerRadius(function(d, i) { return d.outer })
				.startAngle(3 * Math.PI / 2)
				.endAngle(2 * Math.PI);

	// Top-level container for all SVG radar blips
	svg.append("g")
			.attr("class", "radar-blips");
}

function render_radar(error, data)
{
	console.log(error);
	if(error)
		return;

//	d3.select("#title").text(data.name);

	arcs = data.arcs;

	svg.select("g.arcs")
		.selectAll("g").remove();

	var gArc = svg.select("g.arcs")
				.selectAll("g")
				.data(data.arcs, function(d) { return d.name } );

	gArc.enter()
		.append("g")
		.append("svg:path")
		.attr("transform", "translate(" + cfg.width + ", " + (cfg.height - cfg.YOffset) + ")")
		.attr("class", function(d) {
			if( d.class != null ) { return d.class }
			return d.name.toLowerCase();
		})
		.attr("fill", function(d) { return d.fill; })
		.attr("stroke", "white")
		.attr("d", arcPathFn)
		.classed("arc", true);

	// Arc Labels
	gArc.enter()
		.append("g")
		.attr("transform", function(d) {
				var x = cfg.width - d.outer + ((d.outer - d.inner)/2);
				var y = cfg.height - cfg.labelOffset;

				return "translate(" + x + "," + y + ")";
			})
		.append("svg:text")
		.text(function(d) { return d.name } )
		.attr("class", "arc-text")
		.attr("font-size", cfg.labelSize)
		.attr("font-family", cfg.labelFamily)
		.attr("text-anchor", "middle")
		.attr("fill", "black");

	gArc.exit()
		.remove();

	var defaultRadar = data.radars[0].radar;
	var hash = window.location.hash;
	console.log("hash is: " + hash);
	var tab = hash.substr(hash.lastIndexOf('#'));
	console.log("tab is: " + tab);

	if ( hash.lastIndexOf('#') != 0 && tab != null && tab != "" )
	{
		defaultRadar = decodeURIComponent(tab.substr(1));
	}

	drawRadar(data.radars, defaultRadar);
}

function getRadarByName(radars, radarName)
{
	for (i in radars)
	{
		var radar = radars[i];
		if (radar.radar == radarName)
			return radar;
	}
	
	console.log("Can't find radar by name: " + radarName);
	return null;
}

function drawRadar(radars, radarName)
{
	var hash = window.location.hash;
	var hashIdx = hash.lastIndexOf('#');
	if ( hashIdx == 0 )
		hashIdx = hash.length;

	var tab = hash.substr(0, hashIdx) + "#" + encodeURIComponent(radarName);
	console.log("Tab is: tab");
	window.location.hash = tab;

	d3.select("#blips").selectAll(".arc").remove();
	d3.select("svg.svg-container g.radar-blips").selectAll("*").remove();

	var radar = getRadarByName(radars, radarName);
	if(radar == null)
		return;

/*
	var arcCategoryDiv = d3.select("#blips")
							.selectAll(".arc")
							.data(radar.data, function(d) { return d.arc });

	arcCategoryDiv.enter()
				.append("div")
				.attr("class", "arc")
				.append("span")
				.attr("class", "arc-name")
				.text(function(d) { return d.arc });

	arcCategoryDiv.exit().remove();

	var blipDivs = arcCategoryDiv.selectAll("div")
					.data(function(d) { return d.items }, function(d) { return d.name });

	blipDivs.exit().remove();

	var bde = blipDivs.enter()
			.append("div")
			.attr("id", function (d) { return "blip-" + d.idx })
			.attr("class", "blip")
			.on("mouseover", function (d,i) {
				var self = this; 
				d3.selectAll("svg g.blip-group g.blip-g")
					.filter(function(x) { return d.idx != x.item.idx })
					.classed("faded", true);

				d3.select("#blip-" + d.idx).classed("highlight", true);
			})
			.on("mouseout", function (d,i) {
				var self = this;

				d3.selectAll("svg g.blip-group g.blip-g")
					.filter(function(x) { return d.idx != x.item.idx })
					.classed("faded", false);

				d3.select("#blip-" + d.idx).classed("highlight", false);
			});

	bde.append("span")
			.attr("class", "blip-num")
			.text(function (d) { return d.idx + "."});

	bde.append("span")
			.attr("class", "blip-txt")
			.text(function (d) { return d.name });

	blipDivs.filter(function(d) { return d.status != "old" })
			.append("span")
			.attr("class", "blip-new")
			.text("new");
	// END DRAW BLIP TEXT
*/
	var blipRoot = d3.select("svg.svg-container g.radar-blips")
			.selectAll("g")
			.data(radar.data, function(d) { return d.arc });

	blipRoot.exit().remove();

//	for ( i in radar.data )
//	{

	var blips = blipRoot.enter()
			.append("g")
			.attr("class", "blip-group")
			.selectAll("g.blip")
			.data(function(d) {
					return d.items.map(function (i) {
						console.log(i);
						return { arc: d.arc, item: i }
					})
				}, function (d) { return d.item.name });

		//var arc = radar.data[i];
		//var blips = blipRoot.selectAll("g.blip")
		//		.data(arc.items);

		var c = blips.enter()
			.append("g")
			.attr("class", "blip-g")
			.attr("transform", function(d) {
				var arcDef = getArc(d.arc);

				if(arcDef == null) { alert("Can't find arc: " + d.arc); return }

				var arcWidth = arcDef.outer - arcDef.inner;
				var positionInArc = ((100 - d.item.confidence) / 100) * (arcWidth*0.7) + (arcWidth*0.15);
				var r = positionInArc + arcDef.inner;

				var angle = (((d.item.theta/90) * cfg.arcSpan) + (90 - cfg.arcSpan)/2) * (Math.PI/2) / 90;

				var cx = cfg.width - (Math.cos(angle) * r);
				var cy = cfg.height - cfg.YOffset - (Math.sin(angle) * r);

				console.log("item.idx: " + d.item.idx);
				console.log("inner: " + arcDef.inner + " outer: " + arcDef.outer);
				console.log("arcWidth: " + arcWidth + " positionInArc: " + positionInArc + " r: " + r + " cx: " + cx + " cy: " + cy);
				return "translate(" + cx + "," + cy + ")";
			})
			.on("mouseover", function (d,i) {
				var self = this;
				d3.selectAll("svg g.blip-group g.blip-g")
					.filter(function(x) { return self != this })
					.classed("faded", true);

				d3.select("#blip-" + d.item.idx).classed("highlight", true);
			})
			.on("mouseout", function (d,i) { 
				var self = this; 
				d3.selectAll("svg g.blip-group g.blip-g")
					.filter(function(x) { return self != this })
					.classed("faded", false);

				d3.select("#blip-" + d.item.idx).classed("highlight", false);
			});

		blips.exit().remove();

		// OLD BLIPS
		c.filter(function (d) { return d.item.status == "old" })
			.append("svg:circle")
			.attr("class","circle")
			.attr("r", cfg.blipRadius)
			.attr("cx", 0)
			.attr("cy", -cfg.blipRadius/4);

		// NEW OR CHANGED BLIPS
		c.filter(function (d) { return d.item.status != "old" })
			.append("svg:path")
			.attr("d", d3.svg.symbol().type("triangle-up").size(1200))
			.attr("class","triangle")
			.attr("stroke-linejoin", "round")
			.attr("transform", "translate(0,-15)");

		c.append("svg:text")
			.text(function(d) { return d.item.idx })
			.attr("fill", "white")
			.attr("font-size", "20px")
			.attr("stroke-width", 0)
			.attr("stroke", "white")
			.attr("text-anchor", "middle");
	//}
}


