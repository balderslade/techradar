function getQuerystringNameValue(name, defaultValue)
{
    // For example... passing a name parameter of "name1" will return a value of "100", etc.
    // page.htm?name1=100&name2=101&name3=102

    var winURL = window.location.href;
    
    if (winURL.indexOf("?") == -1)
    {
        return defaultValue;
    }

    var queryStringArray = winURL.split("?");
    
    if (!queryStringArray ||  queryStringArray[1].indexOf("&") > -1)
    {
        return defaultValue;
    }
    
    var queryStringParamArray = queryStringArray[1].split("&");
    var nameValue = null;

    for ( var i=0; i<queryStringParamArray.length; i++ )
    {           
        queryStringNameValueArray = queryStringParamArray[i].split("=");

        if ( name == queryStringNameValueArray[0] )
        {
            nameValue = queryStringNameValueArray[1];
        }                       
    }

    return nameValue;
}