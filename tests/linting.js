const fs = require('fs');
var jsonlint = require("jsonlint");

var lint_file = function (file, test) {
	var data;

	test.doesNotThrow(function() {data = fs.readFileSync(file, "utf8")} );

	test.doesNotThrow(function() { jsonlint.parse(data) } );
};

exports.tests = {
	"radars.js": function(test) {
		test.expect(2);
		lint_file("radars.js", test );
		test.done();
	},
	"dmgilp/december-2015.js": function(test) {
		test.expect(2);
		lint_file("dmgilp/december-2015.js", test);
		test.done();
	},
	"dmgilp/february-2016.js": function(test) {
		test.expect(2);
		lint_file("dmgilp/february-2016.js", test);
		test.done();
	},
	"frisk/october-2015.js": function(test) {
		test.expect(2);
		lint_file("frisk/october-2015.js", test);
		test.done();
	},
	"frisk/september-2015.js": function(test) {
		test.expect(2);
		lint_file("frisk/september-2015.js", test);
		test.done();
	}
};
	