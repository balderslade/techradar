var app = angular.module('radarApp', ['buttonRow', 'blipList']);

app.controller('RadarChoicesController', [ '$http', '$scope', '$window', '$location', function($http, $scope, $window, $location) {
	var d3 = $window.d3;
	$scope.onDivisionSelect = function() {
		$scope.radar = $scope.division.files[0];
		$scope.onRadarSelect();
	}

	$scope.onRadarSelect = function() {
		$location.path( $scope.division.division + '/' + $scope.radar.label );
		$http.get($scope.radar.file)
			.success(function (data) {

				// number it all up
				angular.forEach(data.radars, function(radar) {
					var idx = 1;
					angular.forEach(radar.data, function(quadrant) {
						angular.forEach(quadrant.items, function (item) {
							item.idx = idx++;
						});
					});
				});

				$scope.radar_data = data;
				var hash = $location.hash();
				if (hash.length > 0) {
					$scope.selectTabByName(hash);
				}
			});
		d3.json($scope.radar.file, render_radar);
	}

	$scope.selectDivisionByName = function(name) {
		angular.forEach($scope.radars, function(division) {
			if (division.division === name) {
				$scope.division = division;
				$scope.onDivisionSelect();
				return;
			}
		});
	}

	$scope.selectRadarByName = function(name) {
		console.log("Finding radar: " + name);
		angular.forEach($scope.division.files, function (file) {
			if (file.label === name) {
				$scope.radar = file;
				$scope.onRadarSelect();
				return;
			}
		});
	}

	$scope.selectTabByName = function(name) {
		angular.forEach($scope.radar_data.radars, function (quadrant) {
			if (quadrant.radar === name) {
				$scope.selectTab(quadrant);
			}
		});
	}

	$scope.selectTab = function(quadrant) {
		angular.forEach($scope.radar_data.radars, function (quadrant) {
			quadrant.selected = false;
		});
		console.log("Selecting quadrant: " + quadrant.radar);
		quadrant.selected = true;
		$scope.quadrant = quadrant;

		drawRadar($scope.radar_data.radars, quadrant.radar)
	}

	$scope.blipMouseEnter = function(item) {
		item.hover = true;

		d3.selectAll("svg g.blip-group g.blip-g")
			.filter(function(x) { 
				return item.idx != x.item.idx })
			.classed("faded", true);
	}

	$scope.blipMouseLeave = function(item) {
		item.hover = false;

		d3.selectAll("svg g.blip-group g.blip-g")
			.classed("faded", false);
	}
	
	$http.get('radars.js')
	.success(function(data, status) {
		$scope.radars = data;

		if ( $location.path().length != 0 )
		{
			var components = $location.path().split('/');
			$scope.selectDivisionByName(components[1]);
			$scope.selectRadarByName(components[2]);
		}
		else {
			$scope.division = $scope.radars[0];
			$scope.onDivisionSelect();
		}
	})
	.error(function(data, status) {
		console.log("Couldn't retrieve radar json: " + status);
	});
}]);

app.controller('BlipListController',
	[ '$http', '$scope', '$window', '$location', 'Data',
	function($http, $scope, $window, $location, Data) {
		$scope = Data;
		
		var d3 = $window.d3;

		alert("hi there!");
}]);