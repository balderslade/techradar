angular.module('blipList', [])
	.directive('blipList', function($window, $location) {
		return {
			restrict: 'E',
			controller: [ '$http', '$scope', '$window', '$location',
				function($http, $scope, $window, $location) {
				}],
			templateUrl: 'components/blip-list.html',
			replace: true
		};
	});