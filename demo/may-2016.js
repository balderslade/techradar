{
	"name": "Demo Technology Radar - May 2016",
	"arcs": [
		{
			"name": "Adopt",
			"inner": 0,
			"outer": 409
		},
		{
			"name": "Trial",
			"inner": 409,
			"outer": 613
		},
		{
			"name": "Assess",
			"inner": 613,
			"outer": 766
		},
		{
			"name": "Hold",
			"inner": 766,
			"outer": 920
		}
	],

	"radars": [
		{
			"radar": "Languages & Frameworks",
			"data": [
				{
					"arc": "Adopt",
					"items": [
						{ "name": "C#", "confidence": 65, "theta": 5, "status": "old" },						
						{ "name": "RESTful APIs", "confidence": 10, "theta": 40, "status": "new" }
					]
				},
				{
					"arc": "Trial",
					"items": [
						{ "name": "Angular 1.x", "confidence": 80, "theta": 60, "status": "old" }
					]
				},
				{
					"arc": "Assess",
					"items": [
						{ "name": "ECMAScript v6", "confidence": 50, "theta": 30, "status": "new" }
					]
				},
				{
					"arc": "Hold",
					"items": [
						{ "name": "Typescript", "confidence": 50, "theta": 30, "status": "old" }
					]
				}
			]
		},
		{
			"radar": "Platforms",
			"data": [
				{
					"arc": "Adopt",
					"items": [
						{ "name": "Windows Server 2012 R2", "confidence": 80, "theta": 40, "status": "old" }
					]
				},
				{
					"arc": "Hold",
					"items": [
						{ "name": "Windows Server 2003", "confidence": 80, "theta": 5, "status": "old" }
					]
				}
			]
		}
	]
}